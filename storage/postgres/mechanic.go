package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/user_service/genproto/user_service"
	"gitlab.com/car_rental/user_service/pkg/helper"
)

type MechanicRepo struct {
	db *pgxpool.Pool
}

func NewMechanicRepo(db *pgxpool.Pool) *MechanicRepo {
	return &MechanicRepo{
		db: db,
	}
}

func (r *MechanicRepo) Create(ctx context.Context, req *user_service.MechanicCreate) (*user_service.Mechanic, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO mechanic(id, name, birthday, phone_number, updated_at)
		VALUES ($1, $2, $3, $4, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Birthday,
		req.PhoneNumber,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.Mechanic{
		Id:          id,
		Name:        req.Name,
		Birthday:    req.Birthday,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *MechanicRepo) GetByID(ctx context.Context, req *user_service.MechanicPrimaryKey) (*user_service.Mechanic, error) {

	var (
		query string

		id          sql.NullString
		name        sql.NullString
		birthday    sql.NullString
		phoneNumber sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	query = `
		SELECT
			id,	name, birthday, phone_number, created_at, updated_at		
		FROM mechanic
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&birthday,
		&phoneNumber,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.Mechanic{
		Id:          id.String,
		Name:        name.String,
		Birthday:    birthday.String,
		PhoneNumber: phoneNumber.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}, nil
}

func (r *MechanicRepo) GetList(ctx context.Context, req *user_service.MechanicGetListRequest) (*user_service.MechanicGetListResponse, error) {

	var (
		resp   = &user_service.MechanicGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			birthday,
			phone_number,
			created_at,
			updated_at		
		FROM mechanic
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			birthday    sql.NullString
			phoneNumber sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&birthday,
			&phoneNumber,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Mechanics = append(resp.Mechanics, &user_service.Mechanic{
			Id:          id.String,
			Name:        name.String,
			Birthday:    birthday.String,
			PhoneNumber: phoneNumber.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *MechanicRepo) Update(ctx context.Context, req *user_service.MechanicUpdate) (*user_service.Mechanic, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			mechanic
		SET
			name = :name,
			birthday = :birthday,
			phone_number = :phone_number,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"birthday":     req.Birthday,
		"phone_number": req.PhoneNumber,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &user_service.Mechanic{
		Id:          req.Id,
		Name:        req.Name,
		Birthday:    req.Birthday,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *MechanicRepo) Delete(ctx context.Context, req *user_service.MechanicPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM mechanic WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
