package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/user_service/genproto/user_service"
	"gitlab.com/car_rental/user_service/pkg/helper"
)

type ClientRepo struct {
	db *pgxpool.Pool
}

func NewClientRepo(db *pgxpool.Pool) *ClientRepo {
	return &ClientRepo{
		db: db,
	}
}

func (r *ClientRepo) Create(ctx context.Context, req *user_service.ClientCreate) (*user_service.Client, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO client(id, name, surname, middle_name,phone_number,address,jshir_number, balance,email,updated_at)
		VALUES ($1, $2, $3, $4, $5,$6,$7, $8,$9,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Surname,
		req.MiddleName,
		req.PhoneNumber,
		req.Address,
		req.JshShir,
		req.Balance,
		req.Email,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.Client{
		Id:          id,
		Name:        req.Name,
		Surname:     req.Surname,
		MiddleName:  req.MiddleName,
		PhoneNumber: req.PhoneNumber,
		Address:     req.Address,
		JshShir:     req.JshShir,
		Balance:     req.Balance,
		Email:       req.Email,
	}, nil
}

func (r *ClientRepo) GetByID(ctx context.Context, req *user_service.ClientPrimaryKey) (*user_service.Client, error) {

	// var whereField = "id"
	// if len(req.Username) > 0 {
	// 	whereField = "username"
	// 	req.Id = req.Username
	// }

	var (
		query string

		id           sql.NullString
		name         sql.NullString
		surname      sql.NullString
		middle_name  sql.NullString
		phone_number sql.NullString
		address      sql.NullString
		jshShir      sql.NullString
		balance      sql.NullInt64
		email        sql.NullString
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			surname,
			middle_name,
			phone_number,
			address,
			jshir_number,
			balance,
			email,
			created_at,
			updated_at		
		FROM client
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&surname,
		&middle_name,
		&phone_number,
		&address,
		&jshShir,
		&balance,
		&email,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.Client{
		Id:          id.String,
		Name:        name.String,
		Surname:     surname.String,
		MiddleName:  middle_name.String,
		PhoneNumber: phone_number.String,
		Address:     address.String,
		JshShir:     jshShir.String,
		Balance:     balance.Int64,
		Email:       email.String,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (r *ClientRepo) GetList(ctx context.Context, req *user_service.ClientGetListRequest) (*user_service.ClientGetListResponse, error) {

	var (
		resp   = &user_service.ClientGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			surname,
			middle_name,
			phone_number,
			address,
			jshir_number,
			balance,
			email,
			created_at,
			updated_at		
		FROM client
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%' AND surname ILIKE '%' || '` + req.Search + `' || '%' AND middle_name ILIKE '%' || '` + req.Search + `' || '%'`
	}
	if req.SearchByPhoneNumber != "" {
		where += ` AND phone_number ILIKE '%' || '` + req.SearchByPhoneNumber + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			surname      sql.NullString
			middle_name  sql.NullString
			phone_number sql.NullString
			address      sql.NullString
			jshir_number sql.NullString
			balance      sql.NullInt64
			email        sql.NullString
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&surname,
			&middle_name,
			&phone_number,
			&address,
			&jshir_number,
			&balance,
			&email,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Clients = append(resp.Clients, &user_service.Client{
			Id:          id.String,
			Name:        name.String,
			Surname:     surname.String,
			MiddleName:  middle_name.String,
			PhoneNumber: phone_number.String,
			Address:     address.String,
			JshShir:     jshir_number.String,
			Balance:     balance.Int64,
			Email:       email.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ClientRepo) Update(ctx context.Context, req *user_service.ClientUpdate) (*user_service.Client, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			client
		SET
			name = :name,
			surname = :surname,
			middle_name = :middle_name,
			phone_number = :phone_number,
			address = :address,
			jshir_number = :jshir_number,
			balance = :balance,
			email = :email,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"surname":      req.Surname,
		"middle_name":  req.MiddleName,
		"phone_number": req.PhoneNumber,
		"address":      req.Address,
		"jshir_number": req.JshShir,
		"balance":      req.Balance,
		"email":        req.Email,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &user_service.Client{
		Id:          req.Id,
		Name:        req.Name,
		Surname:     req.Surname,
		MiddleName:  req.MiddleName,
		PhoneNumber: req.PhoneNumber,
		Address:     req.Address,
		JshShir:     req.JshShir,
		Balance:     req.Balance,
		Email:       req.Email,
	}, nil
}

func (r *ClientRepo) Delete(ctx context.Context, req *user_service.ClientPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM client WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
