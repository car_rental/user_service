package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/user_service/genproto/user_service"
	"gitlab.com/car_rental/user_service/pkg/helper"
)

type InvestorRepo struct {
	db *pgxpool.Pool
}

func NewInvestorRepo(db *pgxpool.Pool) *InvestorRepo {
	return &InvestorRepo{
		db: db,
	}
}

func (r *InvestorRepo) Create(ctx context.Context, req *user_service.InvestorCreate) (*user_service.Investor, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO investor(id, name, birthday, phone_number, balance,updated_at)
		VALUES ($1, $2, $3, $4, $5, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Birthday,
		req.PhoneNumber,
		req.Balance,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.Investor{
		Id:          id,
		Name:        req.Name,
		Birthday:    req.Birthday,
		PhoneNumber: req.PhoneNumber,
		Balance:     req.Balance,
	}, nil
}

func (r *InvestorRepo) GetByID(ctx context.Context, req *user_service.InvestorPrimaryKey) (*user_service.Investor, error) {

	// var whereField = "id"
	// if len(req.Username) > 0 {
	// 	whereField = "username"
	// 	req.Id = req.Username
	// }

	var (
		query string

		id           sql.NullString
		name         sql.NullString
		birthday     sql.NullString
		phone_number sql.NullString
		balance      sql.NullInt64
		percent      sql.NullInt64
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			birthday,
			phone_number,
			balance,
			percent,
			created_at,
			updated_at		
		FROM investor
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&birthday,
		&phone_number,
		&balance,
		&percent,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.Investor{
		Id:          id.String,
		Name:        name.String,
		Birthday:    birthday.String,
		PhoneNumber: phone_number.String,
		Balance:     balance.Int64,
		Percent:     percent.Int64,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (r *InvestorRepo) GetList(ctx context.Context, req *user_service.InvestorGetListRequest) (*user_service.InvestorGetListResponse, error) {

	var (
		resp   = &user_service.InvestorGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			birthday,
			phone_number,
			balance,
			percent,
			created_at,
			updated_at		
		FROM investor
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	if req.SearchByPhoneNumber != "" {
		where += ` AND phone_number ILIKE '%' || '` + req.SearchByPhoneNumber + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			birthday     sql.NullString
			phone_number sql.NullString
			balance      sql.NullInt64
			percent      sql.NullInt64
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&birthday,
			&phone_number,
			&balance,
			&percent,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Investors = append(resp.Investors, &user_service.Investor{
			Id:          id.String,
			Name:        name.String,
			Birthday:    birthday.String,
			PhoneNumber: phone_number.String,
			Balance:     balance.Int64,
			Percent:     percent.Int64,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *InvestorRepo) Update(ctx context.Context, req *user_service.InvestorUpdate) (*user_service.Investor, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			investor
		SET
			name = :name,
			birthday = :birthday,
			phone_number = :phone_number,
			balance = :balance,
			percent = :percent,		
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"birthday":     req.Birthday,
		"phone_number": req.PhoneNumber,
		"balance":      req.Balance,
		"percent":      req.Percent,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &user_service.Investor{
		Id:          req.Id,
		Name:        req.Name,
		PhoneNumber: req.PhoneNumber,
		Balance:     req.Balance,
		Percent:     req.Percent,
	}, nil
}

func (r *InvestorRepo) Delete(ctx context.Context, req *user_service.InvestorPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM investor WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
