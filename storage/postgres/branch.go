package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/user_service/genproto/user_service"
	"gitlab.com/car_rental/user_service/pkg/helper"
)

type BranchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) *BranchRepo {
	return &BranchRepo{
		db: db,
	}
}

func (r *BranchRepo) Create(ctx context.Context, req *user_service.BranchCreate) (*user_service.Branch, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO branch(id, name, address, phone_number, updated_at)
		VALUES ($1, $2, $3, $4, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Address,
		req.PhoneNumber,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.Branch{
		Id:          id,
		Name:        req.Name,
		Address:     req.Address,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *BranchRepo) GetByID(ctx context.Context, req *user_service.BranchPrimaryKey) (*user_service.Branch, error) {

	var (
		query string

		id           sql.NullString
		name         sql.NullString
		address      sql.NullString
		phone_number sql.NullString
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			address,
			phone_number,
			created_at,
			updated_at		
		FROM branch
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&address,
		&phone_number,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.Branch{
		Id:          id.String,
		Name:        name.String,
		Address:     address.String,
		PhoneNumber: phone_number.String,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (r *BranchRepo) GetList(ctx context.Context, req *user_service.BranchGetListRequest) (*user_service.BranchGetListResponse, error) {

	var (
		resp   = &user_service.BranchGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			address,
			phone_number,
			created_at,
			updated_at		
		FROM branch
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			address      sql.NullString
			phone_number sql.NullString
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&address,
			&phone_number,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Branches = append(resp.Branches, &user_service.Branch{
			Id:          id.String,
			Name:        name.String,
			Address:     address.String,
			PhoneNumber: phone_number.String,

			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *BranchRepo) Update(ctx context.Context, req *user_service.BranchUpdate) (*user_service.Branch, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			investor
		SET
			name = :name,
			address = :address,
			phone_number = :phone_number,
			balance = :balance,
			percent = :percent,		
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"address":      req.Address,
		"phone_number": req.PhoneNumber,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &user_service.Branch{
		Id:          req.Id,
		Name:        req.Name,
		Address:     req.Address,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *BranchRepo) Delete(ctx context.Context, req *user_service.BranchPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM branch WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
