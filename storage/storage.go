package storage

import (
	"context"

	"gitlab.com/car_rental/user_service/genproto/user_service"
)

type StorageI interface {
	CloseDB()
	Investor() InvestorRepoI
	Branch() BranchRepoI
	Client() ClientRepoI
	Mechanic() MechanicRepoI
}

type InvestorRepoI interface {
	Create(context.Context, *user_service.InvestorCreate) (*user_service.Investor, error)
	GetByID(context.Context, *user_service.InvestorPrimaryKey) (*user_service.Investor, error)
	GetList(context.Context, *user_service.InvestorGetListRequest) (*user_service.InvestorGetListResponse, error)
	Update(context.Context, *user_service.InvestorUpdate) (*user_service.Investor, error)
	Delete(context.Context, *user_service.InvestorPrimaryKey) error
}

type BranchRepoI interface {
	Create(context.Context, *user_service.BranchCreate) (*user_service.Branch, error)
	GetByID(context.Context, *user_service.BranchPrimaryKey) (*user_service.Branch, error)
	GetList(context.Context, *user_service.BranchGetListRequest) (*user_service.BranchGetListResponse, error)
	Update(context.Context, *user_service.BranchUpdate) (*user_service.Branch, error)
	Delete(context.Context, *user_service.BranchPrimaryKey) error
}

type ClientRepoI interface {
	Create(context.Context, *user_service.ClientCreate) (*user_service.Client, error)
	GetByID(context.Context, *user_service.ClientPrimaryKey) (*user_service.Client, error)
	GetList(context.Context, *user_service.ClientGetListRequest) (*user_service.ClientGetListResponse, error)
	Update(context.Context, *user_service.ClientUpdate) (*user_service.Client, error)
	Delete(context.Context, *user_service.ClientPrimaryKey) error
}

type MechanicRepoI interface {
	Create(context.Context, *user_service.MechanicCreate) (*user_service.Mechanic, error)
	GetByID(context.Context, *user_service.MechanicPrimaryKey) (*user_service.Mechanic, error)
	GetList(context.Context, *user_service.MechanicGetListRequest) (*user_service.MechanicGetListResponse, error)
	Update(context.Context, *user_service.MechanicUpdate) (*user_service.Mechanic, error)
	Delete(context.Context, *user_service.MechanicPrimaryKey) error
}
