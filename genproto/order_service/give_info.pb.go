// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: give_info.proto

package order_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Give_Info struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	OrderId    string `protobuf:"bytes,2,opt,name=order_id,json=orderId,proto3" json:"order_id,omitempty"`
	MechanikId string `protobuf:"bytes,3,opt,name=mechanik_id,json=mechanikId,proto3" json:"mechanik_id,omitempty"`
	Mileage    int64  `protobuf:"varint,4,opt,name=mileage,proto3" json:"mileage,omitempty"`
	Gas        int64  `protobuf:"varint,5,opt,name=gas,proto3" json:"gas,omitempty"`
	Photo      string `protobuf:"bytes,6,opt,name=photo,proto3" json:"photo,omitempty"`
	CreatedAt  string `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt  string `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *Give_Info) Reset() {
	*x = Give_Info{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_info_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Give_Info) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Give_Info) ProtoMessage() {}

func (x *Give_Info) ProtoReflect() protoreflect.Message {
	mi := &file_give_info_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Give_Info.ProtoReflect.Descriptor instead.
func (*Give_Info) Descriptor() ([]byte, []int) {
	return file_give_info_proto_rawDescGZIP(), []int{0}
}

func (x *Give_Info) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Give_Info) GetOrderId() string {
	if x != nil {
		return x.OrderId
	}
	return ""
}

func (x *Give_Info) GetMechanikId() string {
	if x != nil {
		return x.MechanikId
	}
	return ""
}

func (x *Give_Info) GetMileage() int64 {
	if x != nil {
		return x.Mileage
	}
	return 0
}

func (x *Give_Info) GetGas() int64 {
	if x != nil {
		return x.Gas
	}
	return 0
}

func (x *Give_Info) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

func (x *Give_Info) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *Give_Info) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type Give_InfoPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *Give_InfoPrimaryKey) Reset() {
	*x = Give_InfoPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_info_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Give_InfoPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Give_InfoPrimaryKey) ProtoMessage() {}

func (x *Give_InfoPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_give_info_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Give_InfoPrimaryKey.ProtoReflect.Descriptor instead.
func (*Give_InfoPrimaryKey) Descriptor() ([]byte, []int) {
	return file_give_info_proto_rawDescGZIP(), []int{1}
}

func (x *Give_InfoPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type Give_InfoCreate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	OrderId    string `protobuf:"bytes,1,opt,name=order_id,json=orderId,proto3" json:"order_id,omitempty"`
	MechanikId string `protobuf:"bytes,2,opt,name=mechanik_id,json=mechanikId,proto3" json:"mechanik_id,omitempty"`
	Mileage    int64  `protobuf:"varint,3,opt,name=mileage,proto3" json:"mileage,omitempty"`
	Gas        int64  `protobuf:"varint,4,opt,name=gas,proto3" json:"gas,omitempty"`
	Photo      string `protobuf:"bytes,5,opt,name=photo,proto3" json:"photo,omitempty"`
}

func (x *Give_InfoCreate) Reset() {
	*x = Give_InfoCreate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_info_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Give_InfoCreate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Give_InfoCreate) ProtoMessage() {}

func (x *Give_InfoCreate) ProtoReflect() protoreflect.Message {
	mi := &file_give_info_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Give_InfoCreate.ProtoReflect.Descriptor instead.
func (*Give_InfoCreate) Descriptor() ([]byte, []int) {
	return file_give_info_proto_rawDescGZIP(), []int{2}
}

func (x *Give_InfoCreate) GetOrderId() string {
	if x != nil {
		return x.OrderId
	}
	return ""
}

func (x *Give_InfoCreate) GetMechanikId() string {
	if x != nil {
		return x.MechanikId
	}
	return ""
}

func (x *Give_InfoCreate) GetMileage() int64 {
	if x != nil {
		return x.Mileage
	}
	return 0
}

func (x *Give_InfoCreate) GetGas() int64 {
	if x != nil {
		return x.Gas
	}
	return 0
}

func (x *Give_InfoCreate) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

type Give_InfoUpdate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	OrderId    string `protobuf:"bytes,2,opt,name=order_id,json=orderId,proto3" json:"order_id,omitempty"`
	MechanikId string `protobuf:"bytes,3,opt,name=mechanik_id,json=mechanikId,proto3" json:"mechanik_id,omitempty"`
	Mileage    int64  `protobuf:"varint,4,opt,name=mileage,proto3" json:"mileage,omitempty"`
	Gas        int64  `protobuf:"varint,5,opt,name=gas,proto3" json:"gas,omitempty"`
	Photo      string `protobuf:"bytes,6,opt,name=photo,proto3" json:"photo,omitempty"`
}

func (x *Give_InfoUpdate) Reset() {
	*x = Give_InfoUpdate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_info_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Give_InfoUpdate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Give_InfoUpdate) ProtoMessage() {}

func (x *Give_InfoUpdate) ProtoReflect() protoreflect.Message {
	mi := &file_give_info_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Give_InfoUpdate.ProtoReflect.Descriptor instead.
func (*Give_InfoUpdate) Descriptor() ([]byte, []int) {
	return file_give_info_proto_rawDescGZIP(), []int{3}
}

func (x *Give_InfoUpdate) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Give_InfoUpdate) GetOrderId() string {
	if x != nil {
		return x.OrderId
	}
	return ""
}

func (x *Give_InfoUpdate) GetMechanikId() string {
	if x != nil {
		return x.MechanikId
	}
	return ""
}

func (x *Give_InfoUpdate) GetMileage() int64 {
	if x != nil {
		return x.Mileage
	}
	return 0
}

func (x *Give_InfoUpdate) GetGas() int64 {
	if x != nil {
		return x.Gas
	}
	return 0
}

func (x *Give_InfoUpdate) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

type Give_InfoGetListRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *Give_InfoGetListRequest) Reset() {
	*x = Give_InfoGetListRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_info_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Give_InfoGetListRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Give_InfoGetListRequest) ProtoMessage() {}

func (x *Give_InfoGetListRequest) ProtoReflect() protoreflect.Message {
	mi := &file_give_info_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Give_InfoGetListRequest.ProtoReflect.Descriptor instead.
func (*Give_InfoGetListRequest) Descriptor() ([]byte, []int) {
	return file_give_info_proto_rawDescGZIP(), []int{4}
}

func (x *Give_InfoGetListRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *Give_InfoGetListRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *Give_InfoGetListRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type Give_InfoGetListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count      int64        `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	Give_Infos []*Give_Info `protobuf:"bytes,2,rep,name=Give_Infos,json=GiveInfos,proto3" json:"Give_Infos,omitempty"`
}

func (x *Give_InfoGetListResponse) Reset() {
	*x = Give_InfoGetListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_info_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Give_InfoGetListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Give_InfoGetListResponse) ProtoMessage() {}

func (x *Give_InfoGetListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_give_info_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Give_InfoGetListResponse.ProtoReflect.Descriptor instead.
func (*Give_InfoGetListResponse) Descriptor() ([]byte, []int) {
	return file_give_info_proto_rawDescGZIP(), []int{5}
}

func (x *Give_InfoGetListResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *Give_InfoGetListResponse) GetGive_Infos() []*Give_Info {
	if x != nil {
		return x.Give_Infos
	}
	return nil
}

var File_give_info_proto protoreflect.FileDescriptor

var file_give_info_proto_rawDesc = []byte{
	0x0a, 0x0f, 0x67, 0x69, 0x76, 0x65, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x12, 0x0d, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x22, 0xd7, 0x01, 0x0a, 0x09, 0x47, 0x69, 0x76, 0x65, 0x5f, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x19,
	0x0a, 0x08, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x6d, 0x65, 0x63,
	0x68, 0x61, 0x6e, 0x69, 0x6b, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a,
	0x6d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x6b, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x69,
	0x6c, 0x65, 0x61, 0x67, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x6d, 0x69, 0x6c,
	0x65, 0x61, 0x67, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x67, 0x61, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x03, 0x67, 0x61, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x12, 0x1d, 0x0a, 0x0a,
	0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x25, 0x0a, 0x13, 0x47, 0x69,
	0x76, 0x65, 0x5f, 0x49, 0x6e, 0x66, 0x6f, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65,
	0x79, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69,
	0x64, 0x22, 0x8f, 0x01, 0x0a, 0x0f, 0x47, 0x69, 0x76, 0x65, 0x5f, 0x49, 0x6e, 0x66, 0x6f, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x19, 0x0a, 0x08, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x49, 0x64,
	0x12, 0x1f, 0x0a, 0x0b, 0x6d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x6b, 0x5f, 0x69, 0x64, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x6d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x6b, 0x49,
	0x64, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61, 0x67, 0x65, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61, 0x67, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x67,
	0x61, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x03, 0x67, 0x61, 0x73, 0x12, 0x14, 0x0a,
	0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x68,
	0x6f, 0x74, 0x6f, 0x22, 0x9f, 0x01, 0x0a, 0x0f, 0x47, 0x69, 0x76, 0x65, 0x5f, 0x49, 0x6e, 0x66,
	0x6f, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x6d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x6b, 0x5f, 0x69,
	0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x6d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69,
	0x6b, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61, 0x67, 0x65, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61, 0x67, 0x65, 0x12, 0x10, 0x0a,
	0x03, 0x67, 0x61, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x03, 0x67, 0x61, 0x73, 0x12,
	0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05,
	0x70, 0x68, 0x6f, 0x74, 0x6f, 0x22, 0x5f, 0x0a, 0x17, 0x47, 0x69, 0x76, 0x65, 0x5f, 0x49, 0x6e,
	0x66, 0x6f, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69,
	0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16,
	0x0a, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06,
	0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x69, 0x0a, 0x18, 0x47, 0x69, 0x76, 0x65, 0x5f, 0x49,
	0x6e, 0x66, 0x6f, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x37, 0x0a, 0x0a, 0x47, 0x69, 0x76, 0x65,
	0x5f, 0x49, 0x6e, 0x66, 0x6f, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x18, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x69, 0x76,
	0x65, 0x5f, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x09, 0x47, 0x69, 0x76, 0x65, 0x49, 0x6e, 0x66, 0x6f,
	0x73, 0x42, 0x18, 0x5a, 0x16, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_give_info_proto_rawDescOnce sync.Once
	file_give_info_proto_rawDescData = file_give_info_proto_rawDesc
)

func file_give_info_proto_rawDescGZIP() []byte {
	file_give_info_proto_rawDescOnce.Do(func() {
		file_give_info_proto_rawDescData = protoimpl.X.CompressGZIP(file_give_info_proto_rawDescData)
	})
	return file_give_info_proto_rawDescData
}

var file_give_info_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_give_info_proto_goTypes = []interface{}{
	(*Give_Info)(nil),                // 0: order_service.Give_Info
	(*Give_InfoPrimaryKey)(nil),      // 1: order_service.Give_InfoPrimaryKey
	(*Give_InfoCreate)(nil),          // 2: order_service.Give_InfoCreate
	(*Give_InfoUpdate)(nil),          // 3: order_service.Give_InfoUpdate
	(*Give_InfoGetListRequest)(nil),  // 4: order_service.Give_InfoGetListRequest
	(*Give_InfoGetListResponse)(nil), // 5: order_service.Give_InfoGetListResponse
}
var file_give_info_proto_depIdxs = []int32{
	0, // 0: order_service.Give_InfoGetListResponse.Give_Infos:type_name -> order_service.Give_Info
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_give_info_proto_init() }
func file_give_info_proto_init() {
	if File_give_info_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_give_info_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Give_Info); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_info_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Give_InfoPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_info_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Give_InfoCreate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_info_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Give_InfoUpdate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_info_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Give_InfoGetListRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_info_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Give_InfoGetListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_give_info_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_give_info_proto_goTypes,
		DependencyIndexes: file_give_info_proto_depIdxs,
		MessageInfos:      file_give_info_proto_msgTypes,
	}.Build()
	File_give_info_proto = out.File
	file_give_info_proto_rawDesc = nil
	file_give_info_proto_goTypes = nil
	file_give_info_proto_depIdxs = nil
}
