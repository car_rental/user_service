package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/car_rental/user_service/config"
	"gitlab.com/car_rental/user_service/genproto/user_service"
	"gitlab.com/car_rental/user_service/grpc/client"
	"gitlab.com/car_rental/user_service/grpc/service"
	"gitlab.com/car_rental/user_service/pkg/logger"
	"gitlab.com/car_rental/user_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	user_service.RegisterInvestorServiceServer(grpcServer, service.NewInvestorService(cfg, log, strg, srvc))
	user_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	user_service.RegisterClientServiceServer(grpcServer, service.NewClientService(cfg, log, strg, srvc))
	user_service.RegisterMechanicServiceServer(grpcServer, service.NewMechanicService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
