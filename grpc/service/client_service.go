package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/user_service/config"
	"gitlab.com/car_rental/user_service/genproto/user_service"
	"gitlab.com/car_rental/user_service/grpc/client"
	"gitlab.com/car_rental/user_service/pkg/logger"
	"gitlab.com/car_rental/user_service/storage"
)

type ClientService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedClientServiceServer
}

func NewClientService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ClientService {
	return &ClientService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ClientService) Create(ctx context.Context, req *user_service.ClientCreate) (*user_service.Client, error) {
	u.log.Info("====== Client Create ======", logger.Any("req", req))

	resp, err := u.strg.Client().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Client: u.strg.Client().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ClientService) GetById(ctx context.Context, req *user_service.ClientPrimaryKey) (*user_service.Client, error) {
	u.log.Info("====== Client Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Client().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Client Get By ID: u.strg.Client().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ClientService) GetList(ctx context.Context, req *user_service.ClientGetListRequest) (*user_service.ClientGetListResponse, error) {
	u.log.Info("====== Investor Get List ======", logger.Any("req", req))

	resp, err := u.strg.Client().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Client Get List: u.strg.Client().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ClientService) Update(ctx context.Context, req *user_service.ClientUpdate) (*user_service.Client, error) {
	u.log.Info("====== Client Update ======", logger.Any("req", req))

	resp, err := u.strg.Client().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Client Update: u.strg.Client().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ClientService) Delete(ctx context.Context, req *user_service.ClientPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Client Delete ======", logger.Any("req", req))

	err := u.strg.Client().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Client Delete: u.strg.Client().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
