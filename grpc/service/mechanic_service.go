package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/user_service/config"
	"gitlab.com/car_rental/user_service/genproto/user_service"
	"gitlab.com/car_rental/user_service/grpc/client"
	"gitlab.com/car_rental/user_service/pkg/logger"
	"gitlab.com/car_rental/user_service/storage"
)

type MechanicService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedMechanicServiceServer
}

func NewMechanicService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *MechanicService {
	return &MechanicService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *MechanicService) Create(ctx context.Context, req *user_service.MechanicCreate) (*user_service.Mechanic, error) {
	u.log.Info("====== Mechanic Create ======", logger.Any("req", req))

	resp, err := u.strg.Mechanic().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Mechanic: u.strg.Mechanic().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MechanicService) GetById(ctx context.Context, req *user_service.MechanicPrimaryKey) (*user_service.Mechanic, error) {
	u.log.Info("====== Mechanic Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Mechanic().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Mechanic Get By ID: u.strg.Mechanic().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MechanicService) GetList(ctx context.Context, req *user_service.MechanicGetListRequest) (*user_service.MechanicGetListResponse, error) {
	u.log.Info("====== Investor Get List ======", logger.Any("req", req))

	resp, err := u.strg.Mechanic().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Mechanic Get List: u.strg.Mechanic().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MechanicService) Update(ctx context.Context, req *user_service.MechanicUpdate) (*user_service.Mechanic, error) {
	u.log.Info("====== Mechanic Update ======", logger.Any("req", req))

	resp, err := u.strg.Mechanic().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Mechanic Update: u.strg.Mechanic().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MechanicService) Delete(ctx context.Context, req *user_service.MechanicPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Mechanic Delete ======", logger.Any("req", req))

	err := u.strg.Mechanic().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Mechanic Delete: u.strg.Mechanic().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
