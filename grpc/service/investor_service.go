package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/user_service/config"
	"gitlab.com/car_rental/user_service/genproto/user_service"
	"gitlab.com/car_rental/user_service/grpc/client"
	"gitlab.com/car_rental/user_service/pkg/logger"
	"gitlab.com/car_rental/user_service/storage"
)

type InvestorService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedInvestorServiceServer
}

func NewInvestorService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *InvestorService {
	return &InvestorService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *InvestorService) Create(ctx context.Context, req *user_service.InvestorCreate) (*user_service.Investor, error) {
	u.log.Info("====== Investor Create ======", logger.Any("req", req))

	resp, err := u.strg.Investor().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Investor: u.strg.Investor().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *InvestorService) GetById(ctx context.Context, req *user_service.InvestorPrimaryKey) (*user_service.Investor, error) {
	u.log.Info("====== Investor Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Investor().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Investor Get By ID: u.strg.Investor().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *InvestorService) GetList(ctx context.Context, req *user_service.InvestorGetListRequest) (*user_service.InvestorGetListResponse, error) {
	u.log.Info("====== Investor Get List ======", logger.Any("req", req))

	resp, err := u.strg.Investor().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Investor Get List: u.strg.Investor().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *InvestorService) Update(ctx context.Context, req *user_service.InvestorUpdate) (*user_service.Investor, error) {
	u.log.Info("====== Investor Update ======", logger.Any("req", req))

	resp, err := u.strg.Investor().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Investor Update: u.strg.Investor().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *InvestorService) Delete(ctx context.Context, req *user_service.InvestorPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Investor Delete ======", logger.Any("req", req))

	err := u.strg.Investor().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Investor Delete: u.strg.Investor().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
