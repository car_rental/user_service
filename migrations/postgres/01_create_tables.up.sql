CREATE TABLE investor (
    id UUID PRIMARY KEY NOT NULL,
    "name" varchar,
    birthday date,
    phone_number varchar,
    percent int DEFAULT 70,
    balance int,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE client (
    id UUID PRIMARY KEY NOt NULL,
    name varchar,
    surname varchar,
    middle_name varchar,
    phone_number varchar not NULL,
    address varchar,
    JSHIR_number bigint not NULL,
    balance int,
    email varchar not NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);
CREATE TABLE mechanic(
    id UUID PRIMARY KEY not null,
    name varchar,
    birthday date,
    phone_number varchar not null,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE branch (
    id UUID PRIMARY KEY NOT NULL,
    "name" varchar,
    "address" varchar not NULL ,
    phone_number varchar,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);